let subscribedEvents = new Map();

function getPath(node) {
    let ids = [];
    while (node) {
        if (node.attributes) {
            for (let i = 0, attrs = node.attributes, n = attrs.length; i < n; i++) {
                let attrName = attrs[i].nodeName;
                if (attrName.indexOf("_host") === 0) {
                    ids.push(attrName);
                }
            }
        }
        node = node.parentNode;
    }
    ids.reverse();
    return ids;
}

class Component {
    constructor() {
        this.host_id = null;
        this.content_id = null;
    }

    setup(content_id, host_id) {
        this.host_id = host_id;
        this.content_id = content_id;
    }

    getAllHosts() {
        let s = `[${this.host_id}]`;
        return document.querySelectorAll(s);
    }

    /*getAllElements() {
        let s = `[${this.content_id}]`;
        return document.querySelectorAll(s);
    }*/

    getElementsById(id) {
        let s = `[${this.content_id}-${id}]`;
        return document.querySelectorAll(s);
    }

    querySelectorAll(query) {
        query = query.split(",").map((x) => `[${this.content_id}]${x}`).join(",");
        // console.debug("query", query);

        let nodes = document.querySelectorAll(query);
        // console.debug("nodes", nodes);
        return nodes;
    }

    querySelector(query) {
        query = query.split(",").map((x) => `[${this.content_id}]${x}`).join(",");
        // console.debug("query", query);

        let node = document.querySelector(query);
        // console.debug("node", node);
        return node;
    }

    deepQuerySelectorAll(query) {
        let nodes = document.querySelectorAll(query);
        // console.debug("nodes", nodes);
        return nodes;
    }

    deepQuerySelector(query) {
        let node = document.querySelector(query);
        // console.debug("node", node);
        return node;
    }

    /*
    getElementById(id) {
        let els = this.getElementById(id);
        if (els.length !== 1)
            throw new Error("none or more elements returned");
        return els[0];
    }*/

    subscribeEvents(object, name, callback) {
        let path = getPath(object);
        path.push(name);
        path = path.join("/");

        subscribedEvents.set(path, callback);
    }

    publishEvent(object, name) {
        let path = getPath(object);
        path.pop();
        path.push(name);
        path = path.join("/");

        let sub = subscribedEvents.get(path);
        if (sub)
            sub();
    }
}
