from lib.component import Component

from lib.context import Context


class RootComponent(Component):
    def __init__(self, ctx: Context):
        super().__init__(ctx, replace_tag=True)

    @staticmethod
    def get_tag_name():
        return "root"
