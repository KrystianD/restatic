class RootComponent extends Component {
    install() {
        let el = this.getAllHosts()[0];
        console.log("root", el);

        this.subscribeEvents(el, "btn-clicked", function () {
            alert("Root received", this);
        });
    }
}
