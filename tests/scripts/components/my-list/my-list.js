class MyListComponent extends Component {
    install() {
        let els = this.getElementsById("item");

        console.log("MyListComponent.getElementsById = ", els);

        $(els).click(function () {
            console.log(this);
            alert("you clicked: " + this.innerHTML);
        });
    }
}
