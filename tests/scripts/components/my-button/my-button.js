class MyButtonComponent extends Component {
    install() {
        let els = this.getElementsById("button1");

        console.log("MyButtonComponent.getElementsById = ", els);

        $(els).click((el) => {
            this.publishEvent(el.target, "btn-clicked");
        });
    }
}
