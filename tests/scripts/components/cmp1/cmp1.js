class Cmp1Component extends Component {
    install() {
        let els = this.getAllHosts();
        for (let i = 0; i < els.length; i++) {
            let el = els[i];
            this.subscribeEvents(el, "btn-clicked", function () {
                alert("Cmp1 received", this);
            });
        }
    }
}
