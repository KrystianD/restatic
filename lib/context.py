from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from lib.generator import Generator


class Context:
    def __init__(self):
        self.g = None  # type: Generator
        self.page_data = None  # type: Any
        self.route_path_data = None  # type: Any
        self.inner_content = None  # type: str
        self.instance_id = None  # type: str

    @property
    def site_data(self):
        return self.g.site_data
