import os
from typing import Type

from lib.component import Component
from lib.utils import read_file, underscore2camelcase


class ComponentData:
    def __init__(self, generator: 'Generator', name: str, name_dash: str, cls: Type[Component], id: int,
                 component_dir: str):
        self.generator = generator
        self.name = name_dash.replace("-", "").replace("_", "")
        self.name_camelcase = underscore2camelcase(name_dash.replace("-", "_"))
        self.name_dash = name_dash
        self.cls = cls
        self.content_id = "_content-c{}".format(id)
        self.host_id = "_host-c{}".format(id)
        self.parent_component: ComponentData = None

        self.component_dir = component_dir

        self.styles = None  # type: str
        self.compiled_styles = None  # type: str

    def get_path(self) -> str:
        if self.parent_component is None:
            return self.name
        else:
            return self.parent_component.get_path() + "_" + self.name

    def get_style_path(self):
        main_style_path = os.path.join(self.component_dir, self.name_dash + ".scss")
        return main_style_path if os.path.exists(main_style_path) else None

    def has_html_file(self):
        main_html_path = os.path.join(self.component_dir, self.name_dash + ".html")
        return os.path.exists(main_html_path)

    def get_html(self):
        main_html_path = os.path.join(self.component_dir, self.name_dash + ".html")

        if not os.path.exists(main_html_path):
            raise ValueError("HTML file for component {} doesn't exist".format(self.name))

        with open(main_html_path, "rt") as f:
            cnt = f.read()
        return "<component_root>{}</component_root>".format(cnt)

    def get_script(self):
        script_path = os.path.join(self.component_dir, self.name_dash + ".js")
        return read_file(script_path) if os.path.exists(script_path) else None

    def get_script_inline(self):
        script_path = os.path.join(self.component_dir, self.name_dash + ".inline.js")
        return read_file(script_path) if os.path.exists(script_path) else None
