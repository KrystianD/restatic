import copy
import inspect
import re
from typing import TYPE_CHECKING, List, Dict, Type, Any
import os
import traceback

import bs4
import sass
from bs4 import BeautifulSoup as bs

from lib.component import Component, DefaultComponent
from lib.component_data import ComponentData
from lib.context import Context
from lib.page_context import PageContext
from lib.page_meta import RouterEntry
from lib.utils import load_module, read_file, add_selector_to_css

if TYPE_CHECKING:
    from .generator import Generator

script_dir = os.path.dirname(os.path.realpath(__file__))

HTML_TAGS = ["a", "abbr", "acronym", "address", "area", "b", "base", "bdo", "big", "blockquote", "body", "br", "button",
             "caption", "cite", "code", "col", "colgroup", "dd", "del", "dfn", "div", "dl", "DOCTYPE", "dt", "em",
             "fieldset", "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "html", "hr", "i", "img", "input", "ins",
             "kbd", "label", "legend", "li", "link", "map", "meta", "noscript", "object", "ol", "optgroup", "option",
             "p", "param", "pre", "q", "samp", "script", "select", "small", "span", "strong", "style", "sub", "sup",
             "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "tt", "ul", "var", "nav"]

HTML_TAGS += ["template"]


def compile_scss(x):
    styles, global_styles_dir = x
    return sass.compile(string=styles, include_paths=[global_styles_dir])


def create_tree(html):
    return bs(html, "html.parser")


def create_elements_from_html(html):
    html = "<component_root>{}</component_root>".format(html)
    return list(create_tree(html).find("component_root").children)


class VarsStack:
    def __init__(self, initial: Dict[str, Any] = None):
        self.stack: List[Dict[str, Any]] = []

        if initial is not None:
            self.stack.append(initial)

    def push(self):
        self.stack.append({})

    def pop(self):
        self.stack.pop()

    def __setitem__(self, key, value):
        self.stack[-1][key] = value

    def get_top(self):
        items = {}
        for s in self.stack[::-1]:
            for k, v in s.items():
                if k not in items:
                    items[k] = v
        return items


def myeval(code: str, stack: VarsStack, component_data: ComponentData):
    locals = stack.get_top()
    locals["false"] = False
    locals["true"] = True
    try:
        return eval(code, locals)
    except:
        traceback.print_exc()
        raise Exception("Unable to evaluate code: {} (component: {})".format(code, component_data.name))


def parse_text(text, c_vars: VarsStack, component_data: ComponentData):
    if text is None:
        return None

    def sub(m):
        code = m.group(1)
        out = myeval(code, c_vars, component_data=component_data)
        return str(out)

    return re.sub(r"{{(.*?)}}", sub, text)


def is_input_name_valid(text: str):
    if len(text) > 1 and text[0] == "_":
        return False
    if text.startswith(("host_",)):
        return False
    if text in ("class", "style", "ctx"):
        return False
    return True


class ComponentInjector:
    def __init__(self, generator: 'Generator') -> None:
        self.generator = generator

        self.components: List[ComponentData] = []
        self.component_map: Dict[str, ComponentData] = {}
        self.component_map_by_class: Dict[Type[Component], ComponentData] = {}
        self.component_styles: Dict[ComponentData, str] = {}

        self.page_ctx: PageContext = None
        self._component_instance_id: int = None

        self.attrs_mapping: Dict[str, str] = {}
        self.tags_mapping: Dict[str, str] = {}
        self.element_attr_values: Dict[bs4.Tag, Dict[str, Any]] = {}

        self.reset()

    def reset(self):
        self.components: List[ComponentData] = []
        self.component_map: Dict[str, ComponentData] = {}
        self.component_map_by_class: Dict[Type[Component], ComponentData] = {}
        self.component_styles: Dict[ComponentData, str] = {}

        self.page_ctx: PageContext = None

        self.attrs_mapping = {}
        self.element_attr_values = {}

    def get_all_components(self):
        return self.components

    def get_component_by_name(self, name: str, parent_comp_data: ComponentData = None):
        if name is None:
            return None

        if name.startswith("local_"):
            name = name.replace("local_", parent_comp_data.get_path() + "_")

        c = self.component_map.get(name.lower())
        if c is None:
            return None
        self.page_ctx.used_components.add(c)
        return c

    def get_component_data(self, component: Component):
        return self.component_map_by_class[component.__class__]

    def find_components_in_files(self, files: List[str], parent_component: ComponentData = None):
        for file_path in files:
            file_name = os.path.basename(file_path)

            if file_name in ("__pycache__",) or file_name.startswith("."):
                continue

            if os.path.isfile(file_path):
                # component_name = file_name
                self._load_html_only_component(file_path, parent_component=parent_component)

            if os.path.isdir(file_path):
                component_name = file_name

                subdir_file = os.listdir(file_path)
                other_files = [x for x in subdir_file if not x.startswith("{}.".format(component_name))]
                c = self._load_dir_component(file_path, parent_component=parent_component)

                self.find_components_in_files([os.path.join(file_path, x) for x in other_files],
                                              parent_component=c)

    def _load_html_only_component(self, path: str, parent_component: ComponentData):
        cid = len(self.components)

        file_name = os.path.basename(path)
        name, ext = os.path.splitext(file_name)
        if ext == ".html":
            component_tag_name = name.replace("-", "")
            component_tag_name_dash = name

            obj = copy.deepcopy(DefaultComponent)
            c = ComponentData(self.generator,
                              component_tag_name, component_tag_name_dash, obj,
                              cid, os.path.dirname(path))
            if file_name.startswith("_"):
                c.parent_component = parent_component
            self._prepare_component(obj, c)
        else:
            raise ValueError("Non-html file as component: {}".format(path))

    def _load_dir_component(self, path: str, parent_component: ComponentData):
        cid = len(self.components)

        dir_name = os.path.basename(path)
        component_tag_name = dir_name.replace("-", "")
        component_tag_name_dash = dir_name

        html_path = os.path.join(path, dir_name + ".html")
        py_path = os.path.join(path, dir_name + ".py")

        obj = None
        if os.path.exists(py_path):
            m = load_module(py_path)

            for member in inspect.getmembers(m):
                obj = member[1]  # type: Type[Component]
                if inspect.isclass(obj):
                    clstree = inspect.getclasstree([obj])
                    if clstree[0][0] in (Component,):
                        c = ComponentData(self.generator, component_tag_name, component_tag_name_dash, obj, cid,
                                          path)
                        if dir_name.startswith("_"):
                            dir_name = dir_name[1:]
                            c.parent_component = parent_component

                        if "_" in dir_name:
                            raise ValueError("Component dir name should be separated with dash")

                        self._prepare_component(obj, c)
                        return c

        elif os.path.exists(html_path):
            obj = copy.deepcopy(DefaultComponent)
            c = ComponentData(self.generator, component_tag_name, component_tag_name_dash, obj, cid, path)
            if dir_name.startswith("_"):
                c.parent_component = parent_component
            self._prepare_component(obj, c)
            return c

    def _preprocess_html(self, html: str):
        html = html.replace("*reIf", "re-if")
        html = html.replace("*reFor", "re-for")
        html = html.replace("*ngIf", "re-if")
        html = html.replace("*ngFor", "re-for")

        tag_local_open_re = re.compile("^<_([a-zA-Z0-9_]+)")
        tag_local_close_re = re.compile("^</_([a-zA-Z0-9_]+)>")
        attr_re1 = re.compile("(\[([^\]]+)\])\s*=")
        attr_re2 = re.compile("([^ ]*[A-Z]+[^ ]*)\s*=")

        def attr_name_sub1(m):
            attr = m.group(2)
            idx = len(self.attrs_mapping)
            attr_map = "{}__map{}=".format(attr.lower(), idx)
            self.attrs_mapping[idx] = m.group(1)
            return attr_map

        def attr_name_sub2(m):
            attr = m.group(1)
            idx = len(self.attrs_mapping)
            attr_map = "{}__map{}=".format(attr.lower(), idx)
            self.attrs_mapping[idx] = attr
            return attr_map

        def attr_repl1(m):
            out = m.group(0)

            def fn(m):
                return "<LOCAL_" + m.group(1)

            out = tag_local_open_re.sub(fn, out)

            def fn(m):
                return "</LOCAL_" + m.group(1) + ">"

            out = tag_local_close_re.sub(fn, out)

            return out

        def attr_repl2(m):
            out = m.group(0)

            def fn(m):
                return "<LOCAL_" + m.group(1)

            out = tag_local_open_re.sub(fn, out)

            def fn(m):
                return "</LOCAL_" + m.group(1) + ">"

            out = tag_local_close_re.sub(fn, out)

            out = attr_re1.sub(attr_name_sub1, out)
            out = attr_re2.sub(attr_name_sub2, out)
            return out

        html = re.sub("</?[_a-zA-Z0-9]+>", attr_repl1, html)
        html = re.sub("<[_a-zA-Z0-9]+ [^><]+>", attr_repl2, html)

        return html

    def _get_real_attr(self, x: str):
        m = re.match(".*__map(\d+)", x)
        if m:
            return self.attrs_mapping[int(m.group(1))]
        else:
            return x

    def _apply_tag_vars(self, current_component_data, element, vars_stack: VarsStack):
        if isinstance(element, bs4.Comment):
            return []
        elif isinstance(element, bs4.NavigableString):
            if element.string is not None:
                new_tags = create_elements_from_html(
                        parse_text(element.string, vars_stack, component_data=current_component_data))
                for tag in reversed(new_tags):
                    element.insert_after(tag)
                element.extract()
                if len(new_tags) > 1:
                    return new_tags
        elif isinstance(element, bs4.Tag):
            attrs_to_add = {}
            attrs_to_del = set()
            classes_to_add = set()
            for attr_name, v in element.attrs.items():
                if isinstance(v, list):
                    element.attrs[attr_name] = [parse_text(v, vars_stack, component_data=current_component_data) for v
                                                in v]
                elif isinstance(v, str):
                    base_attr_name = attr_name
                    attr_name = self._get_real_attr(attr_name)

                    attrs_to_del.add(base_attr_name)

                    if attr_name.startswith("[") and attr_name.endswith("]"):
                        attr_name = attr_name[1:-1]

                        if attr_name.startswith("class."):
                            class_name = re.sub("^class\.", "", attr_name)
                            val = myeval(v, vars_stack, component_data=current_component_data)
                            if val:
                                classes_to_add.add(class_name)

                        else:
                            attr_name = re.sub("^attr\.", "", attr_name)

                            val = myeval(v, vars_stack, component_data=current_component_data)
                            if val is not None:
                                attrs_to_add[attr_name] = val
                                if id(element) not in self.element_attr_values:
                                    self.element_attr_values[id(element)] = {}
                                self.element_attr_values[id(element)][attr_name] = val
                    else:
                        val = parse_text(v, vars_stack, component_data=current_component_data)
                        attrs_to_add[attr_name] = val
                        if id(element) not in self.element_attr_values:
                            self.element_attr_values[id(element)] = {}
                        self.element_attr_values[id(element)][attr_name] = val

                    if attr_name.startswith("#"):
                        hash_attr = attr_name[1:]
                        attrs_to_add["{}-{}".format(current_component_data.content_id, hash_attr)] = None

            for k in attrs_to_del:
                del element.attrs[k]

            for k, v in attrs_to_add.items():
                if k == "innerHTML":
                    element.clear()
                    vv = create_tree("<div>{}</div>".format(v)).find("div")
                    for vi in list(vv.contents):
                        element.append(vi)
                else:
                    element.attrs[k] = v

            for class_name in classes_to_add:
                element.attrs["class"] = element.attrs.get("class", "") + " " + class_name

        return []

    def _find_components(self, path: str):
        return self.find_components_in_files([os.path.join(path, x) for x in os.listdir(path)])

    def _prepare_component(self, obj, c: ComponentData):

        self.components.append(c)
        self.component_map[c.get_path()] = c
        self.component_map_by_class[obj] = c

        # prepare component
        main_style_path = c.get_style_path()
        inline_styles = ""

        if c.has_html_file():
            html = c.get_html()
            component_root = create_tree(html).find("component_root")
            styles = component_root.select("style[type='text/sass']") + \
                     component_root.select("style[type='text/scss']")
            for style in styles:
                inline_styles += style.string

        c.styles = ""
        c.compiled_styles = ""

        has_main_style_file = main_style_path is not None
        has_inline_styles = len(inline_styles) > 0

        if has_main_style_file and has_inline_styles:
            raise ValueError("component /{}/ cannot have both style file and inline styles".format(c.name))

        if main_style_path:
            c.styles = read_file(main_style_path)

        if has_inline_styles:
            c.styles = inline_styles

    def _compile_component_styles(self):
        from multiprocessing.pool import Pool

        global_styles_dir = self.generator._get_site_file_path("styles")

        p = Pool()

        components_with_styles = [c for c in self.components if len(c.styles) > 0]

        results = p.map(compile_scss, ((c.styles, global_styles_dir) for c in components_with_styles))
        for i, c in enumerate(components_with_styles):
            compiled_css = results[i]
            compiled_css = add_selector_to_css(compiled_css, c.content_id, c.host_id)
            c.compiled_styles = compiled_css + "\n"

        p.close()

    def load_components(self):
        components_dir1 = self.generator._get_site_file_path("components")
        components_dir2 = os.path.join(script_dir, "..", "lib/components")
        self._find_components(components_dir1)
        self._find_components(components_dir2)

        self._compile_component_styles()

    def _process_component_tag(self,
                               current_component_data: ComponentData,
                               cmp_page_data: Any,
                               current_component: Component,
                               element: bs4.Tag,
                               parent_element: bs4.Tag,
                               vars_stack: VarsStack,
                               components_path: List[RouterEntry]):
        if not isinstance(element, bs4.Tag):
            return

        element[current_component_data.content_id] = None

        vars_stack.push()

        self._apply_tag_vars(current_component_data, element, vars_stack)

        if_statement = element.attrs.get("re-if")
        if if_statement is not None:
            del element.attrs["re-if"]
            res = myeval(if_statement, vars_stack, component_data=current_component_data)
            if not res:
                element.extract()
                return

        for_statement = element.attrs.get("re-for")
        if for_statement is not None:
            del element.attrs["re-for"]

            parts = for_statement.split(" ", 3)
            var_name = parts[1]
            var_source = myeval(parts[3], vars_stack, component_data=current_component_data)

            element.extract()
            for item in var_source:
                vars_stack[var_name] = item
                new_element = copy.deepcopy(element)
                self._apply_tag_vars(current_component_data, new_element, vars_stack)
                parent_element.append(new_element)
                self._process_component_tag(current_component_data,
                                            cmp_page_data,
                                            current_component,
                                            new_element,
                                            parent_element,
                                            vars_stack,
                                            components_path)

        else:
            if element.name.lower() == "routeroutlet" and len(components_path) > 0:
                component_name = components_path[0].component_name
                new_component_page_data = components_path[0].component_data
                components_path = components_path[1:]
            else:
                component_name = element.name
                new_component_page_data = None

            # else:
            new_component_data = self.get_component_by_name(component_name, current_component_data)
            if new_component_data is not None:
                self._replace_with_component(new_component_data,
                                             new_component_page_data,
                                             element,
                                             vars_stack,
                                             components_path)
            elif component_name.lower() in HTML_TAGS:

                self._process_tags(current_component_data, cmp_page_data, current_component, element, vars_stack,
                                   components_path)

                # for el in list(element):
                #     self._apply_tag_vars(current_component_data, el, vars_stack)
                #     self._process_component_tag(current_component_data,
                #                                 cmp_page_data,
                #                                 current_component,
                #                                 el,
                #                                 element,
                #                                 vars_stack,
                #                                 components_path)

            else:
                raise ValueError("Unable to find component {} (from component: {})".format(component_name,
                                                                                           current_component_data.name))

        vars_stack.pop()

    def _replace_with_component(self,
                                component_data: ComponentData,
                                component_page_data: Any,
                                element: bs4.Tag,
                                vars_stack: VarsStack,
                                components_path: List[RouterEntry]):
        ctx = Context()
        ctx.g = self.generator
        ctx.page_data = self.page_ctx.meta.page_data
        ctx.route_path_data = component_page_data
        ctx.inner_content = element.decode_contents(formatter="html")
        ctx.instance_id = "_instance-id{}".format(self._component_instance_id)

        self._component_instance_id += 1

        instance = component_data.cls(ctx)

        element_values = self.element_attr_values.get(id(element), {})
        inputs = {k: v for k, v in element_values.items() if is_input_name_valid(k)}

        # remove attributes from component tag except certain elements
        keep = ["style", "class", "id"]
        element.attrs = {k: v for k, v in element.attrs.items() if k in keep or k.startswith("_")}

        # pass inputs to component instance
        instance.set_input(inputs)

        process_html_result = instance.process()

        # add scripts
        script_inline = component_data.get_script_inline()
        if script_inline is not None:
            k = "{}.inline".format(component_data.name)
            self.generator.append_inline_javascript_body_once(k, script_inline)

        # add html
        if process_html_result is None:
            html = self._preprocess_html(component_data.get_html())
        else:
            html = "<component_root>{}</component_root>".format(process_html_result)
        component_root = create_tree(html).find("component_root")

        for el in component_root.select("style[type='text/sass']") + \
                  component_root.select("style[type='text/scss']"):
            el.extract()

        for k, v in instance.host_attr.items():
            component_root.attrs[k] = v

        component_root.attrs = element.attrs
        for class_name in instance.host_classes:
            component_root.attrs["class"] = component_root.attrs.get("class", "") + " " + class_name

        component_root.attrs[component_data.host_id] = None

        component_root.next_sibling = element.next_sibling
        component_root.name = component_data.get_path()

        element.replaceWith(component_root)
        element = component_root

        inner_vars = {k: v for k, v in vars(instance).items() if is_input_name_valid(k)}
        c_methods = inspect.getmembers(instance, lambda x: inspect.ismethod(x))
        inner_vars.update({k: v for (k, v) in c_methods if is_input_name_valid(k)})
        inner_vars["INNER_CONTENT"] = ctx.inner_content
        inner_vars["CONTENT"] = ctx.inner_content
        inner_vars["PAGE_DATA"] = ctx.page_data
        inner_vars["SITE_DATA"] = ctx.site_data
        inner_vars["INSTANCE_ID"] = ctx.instance_id
        inner_vars["PAGE_PATH"] = self.page_ctx.meta.path
        inner_vars_ctx = VarsStack(inner_vars)

        self._process_tags(component_data, component_page_data, instance, component_root, inner_vars_ctx,
                           components_path)

        if instance.replace_tag:
            component_root.replaceWithChildren()

    def _process_tags(self,
                      component_data: ComponentData,
                      component_page_data: Any,
                      instance,
                      element: bs4.Tag,
                      inner_vars_ctx: VarsStack,
                      components_path):
        to_proceed = list(element.children)
        while len(to_proceed) > 0:
            inner_element = to_proceed[0]
            to_proceed.pop(0)

            new_tags = self._apply_tag_vars(component_data, inner_element, inner_vars_ctx)
            if new_tags is not None and len(new_tags) > 0:
                to_proceed += new_tags  # INNER_CONTENT can create new tags
            else:
                self._process_component_tag(component_data,
                                            component_page_data,
                                            instance,
                                            inner_element,
                                            element,
                                            inner_vars_ctx,
                                            components_path)

    def inject(self, page: PageContext, root: bs4.BeautifulSoup):
        self.page_ctx = page
        self._component_instance_id = 1

        for element in root.descendants:
            if element.name == "root":
                components_path = page.meta.components
                c = self.get_component_by_name("root")
                if c is None:
                    raise ValueError("Unable to find Root component")
                self._replace_with_component(c, None, element, VarsStack(), components_path)
                break

        for el in root.select("template"):
            el.replaceWithChildren()
