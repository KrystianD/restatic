import base64
import os
import re
from ctypes import c_void_p, c_size_t
from typing import TYPE_CHECKING
import mimetypes

from wand.api import library
from wand.image import Image

from lib.utils import compute_file_hash, resize_with_aspect, load_cache, save_cache, logger_done, read_binary_file, \
    compute_py_hash
from lib.asset import AssetType, Asset

if TYPE_CHECKING:
    from lib.generator import Generator


class ImageProperties:
    def __init__(self):
        self.path: str = None
        self.width: int = None
        self.height: int = None
        self.format: str = None
        self.quality: str = "high"

    @staticmethod
    def parse(path: str):
        p = ImageProperties()
        p.path = path
        m = re.match("^(.*?)(?:\((.*)\))?$", path)
        if m:
            p.path = m.group(1)
            options = m.group(2)
            if options:
                for option in options.split(","):
                    m = re.match("(?P<width>\d+)?x(?P<height>\d+)?", options)
                    if m:
                        width = m.group("width")
                        height = m.group("height")
                        if width is not None:
                            p.width = int(width)
                        if height is not None:
                            p.height = int(height)

                    if option.lower() == "png":
                        p.format = "PNG"
                    if option.lower() in ("jpg", "jpeg"):
                        p.format = "JPEG"

                    if option.lower() in ("high", "medium", "low", "verylow", "superlow"):
                        p.quality = option.lower()
        return p

    def has_size(self):
        return self.width is not None or self.height is not None


def get_smallest_png(img):
    smallest_data = None
    for c in range(0, 100):
        library.MagickSetCompressionQuality(img.wand, c)
        img_data = img.make_blob(format="png")

        if smallest_data is None:
            smallest_data = img_data
            print("found better c=", c)

        if len(img_data) < len(smallest_data):
            smallest_data = img_data
            print("found better c=", c)

    print("best len", len(smallest_data))
    return smallest_data


library.MagickSetCompressionQuality.argtypes = [c_void_p, c_size_t]


def format_to_extension(format: str):
    return {
        "PNG": ".png",
        "JPEG": ".jpg",
        "SVG": ".svg",
        "GIF": ".gif",
    }[format]


class ImagesCollection:
    def __init__(self, generator: 'Generator'):
        self.generator = generator
        self.images = []
        self.img_data_cache = {}
        self.generated_images_map = {}

    def _get_image_data(self, path: str):
        ip = ImageProperties.parse(path)
        if ip.path in self.img_data_cache:
            return self.img_data_cache[ip.path]
        with Image(filename=ip.path) as img:
            self.img_data_cache[ip.path] = (img.format, img.size)
        return self.img_data_cache[ip.path]

    def get_image_size(self, path: str):
        img = ImageProperties.parse(path)
        _, (width, height) = self._get_image_data(img.path)
        return width, height

    def get_image_with_prop(self, path: str):
        ip = ImageProperties.parse(path)
        return self.get_image(ip.path, ip.width, ip.height, ip.format, ip.quality)

    def get_low_quality_image(self, path: str):
        ip = ImageProperties.parse(path)
        original_image_format, original_image_size = self._get_image_data(ip.path)

        lq_size = (original_image_size[0] / 50, original_image_size[1] / 50)

        return self.get_image(ip.path, lq_size[0], lq_size[1], ip.format, "superlow")

    def is_image(self, path: str):
        ip = ImageProperties.parse(path)
        image_basename = os.path.basename(ip.path)
        image_name, base_ext = os.path.splitext(image_basename)
        return base_ext in (".png", ".jpg", ".jpeg", ".svg", ".ico")

    def get_image(self, path: str, width: int = None, height: int = None, desired_format: str = None,
                  quality: str = "high") -> 'Asset':
        image_basename = os.path.basename(path)
        image_name, base_ext = os.path.splitext(image_basename)

        if base_ext.lower() == ".svg":
            return self.generator.create_asset_from_file(image_basename, path)
        if base_ext.lower() == ".ico":
            return self.generator.create_asset_from_file(image_basename, path, binary=True)
        # if base_ext.lower() == ".gif":
        #     return self.generator.create_asset_from_file(image_basename, path)

        original_image_format, original_image_size = self._get_image_data(path)

        if desired_format is None:
            image_ext = format_to_extension(original_image_format)
        else:
            image_ext = format_to_extension(desired_format)

        original_image_hash = compute_file_hash(path)

        image_meta = (original_image_hash, image_ext, width or -1, height or -1, "", quality)
        original_image_meta_hash = compute_py_hash(image_meta)
        if image_meta in self.generated_images_map:
            return self.generated_images_map[image_meta]

        cached_image = load_cache(image_meta)

        is_raster = image_ext in (".png", ".jpg")
        is_vector = image_ext == ".svg"

        if is_raster:
            if cached_image is None:
                new_image_size = resize_with_aspect(*original_image_size, width, height)

                with logger_done("generating image {} ({})".format(path, image_meta)):
                    with Image(filename=path) as img:
                        if new_image_size != original_image_size:
                            # print("resizing", original_image_size, "to", new_image_size)
                            img.resize(width=new_image_size[0], height=new_image_size[1], filter="catrom")
                        img.strip()

                        if image_ext == ".png":
                            img.interlace = "nointerlace"
                            img_data = get_smallest_png(img)

                        elif image_ext == ".jpg":
                            img.options["jpeg:sampling-factor"] = "4:2:0"
                            img.interlace = "planeinterlace"

                            if quality == "high":
                                img.compression_quality = 85
                            if quality == "medium":
                                img.compression_quality = 75
                            if quality == "low":
                                img.compression_quality = 65
                            if quality == "verylow":
                                img.compression_quality = 45
                            if quality == "superlow":
                                img.compression_quality = 25

                            img_data = img.make_blob(format="jpg")

                        else:
                            assert False

                    save_cache(image_meta, img_data)
            else:
                img_data = cached_image
        else:
            img_data = read_binary_file(path)

        if len(img_data) < 2048:
            inline_data = "data:{};base64,{}".format(
                    mimetypes.types_map[image_ext],
                    base64.b64encode(img_data).decode("ascii"))
            v = Asset(AssetType.Inline, inline_data)
        else:
            tmp_name = "{}.{}{}".format(image_name, original_image_meta_hash, image_ext)
            v = self.generator.create_asset(tmp_name, img_data)

        self.generated_images_map[image_meta] = v
        return v
