import enum


class AssetType(enum.Enum):
    Local = "Local"
    LocalUrl = "LocalUrl"
    Inline = "Inline"
    External = "External"


class Asset:
    def __init__(self, type: AssetType, url: str, *, local_path: str = None):
        self.type = type
        self.url = url
        self.local_path = local_path
