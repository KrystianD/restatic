import os
import re
from typing import List, Union, Any
from urllib.parse import urljoin, urlparse

import bs4
import sass
import yaml
import requests
from bs4 import BeautifulSoup as bs

from lib.asset import AssetType, Asset
from lib.component_data import ComponentData
from lib.component_injector import ComponentInjector
from lib.content_formatter import ContentFormatter
from lib.fonts import FontVariantData, FontData
from lib.images import ImagesCollection, ImageProperties
from lib.page_context import PageContext
from lib.page_meta import PageDescription, RouterEntry
from lib.utils import compute_string_hash, html_minifier, \
    uncss, process_css_file, load_cache, save_cache, write_file, read_file, logger_done, logger_done_multiline, \
    babelify, load_url_text, merge_dict, optimize_css, read_binary_file, prettify_css

script_dir = os.path.dirname(os.path.realpath(__file__))


def new_tag(tag: str, html=None, **attrs):
    tag = bs4.Tag(name=tag, attrs=attrs)
    if html is not None:
        tag.string = html
    return tag


def add_selector_to_tree(root, selector: str):
    for element in root.children:
        if isinstance(element, bs4.Tag):
            element[selector] = selector


class Generator:
    no_cache = False

    def __init__(self, no_cache=False):
        # site structure
        self.site_data: Any = None
        self.site_dir = None
        self.output_dir = None
        self.configuration_name = False
        self.pages = []  # type: List[PageDescription]

        # generator options
        Generator.no_cache = no_cache
        self.options = {
            "add_global_styles": True,
            "defer_global_styles": True,
            "per_page_inline_styles": True,
            "minify_css": False,
            "minify_html": False,
            "minify_js": False,
            "babelify_js": False,
            "prettify_html": False,
            "merge_js": False,
            "fonts": [],
            "scripts": [],
            "generate_index": None,
            "static_url": None,
        }

        self.styles_tags = []

        self.global_styles_content_first_level = None
        self.global_styles_url = None
        self.global_preloaded_scripts = ""
        self.global_external_scripts = []
        self.fonts: List[FontData] = []
        self.page_fonts_preload_link: List[FontData] = []
        self.page_fonts_preload_inline: List[FontData] = []
        self.sitemap_urlset = None

        self.page_ctx: PageContext = None

        # tools
        self.content_formatter = ContentFormatter.create_default()
        self._component_injector = ComponentInjector(self)
        self.ic = ImagesCollection(self)

    # public
    def add_page(self, path: str, components: List[RouterEntry] = None) -> PageDescription:
        page_desc = PageDescription()
        page_desc.path = path
        if components is not None:
            page_desc.set_router_entries(components)
        self.pages.append(page_desc)
        return page_desc

    def set_site_data(self, data: Any):
        self.site_data = data

    def set_site_dir(self, site_dir: str):
        self.site_dir = site_dir

        if not os.path.exists(self.site_dir):
            raise ValueError("Source directory doesn't exist")

        if not os.path.exists(self._get_site_file_path("site.yaml")):
            raise ValueError("Source directory doesn't contain valid site")

    def set_output_dir(self, output_dir: str):
        self.output_dir = output_dir

    def set_configuration(self, configuration_name: str):
        self.configuration_name = configuration_name

    def append_inline_asset_javascript_head(self, name: str):
        if name in self.page_ctx.added_scripts:
            return
        script_path = os.path.join(script_dir, "../scripts", name + ".js")
        e = bs4.Tag(name="script")
        e.string = read_file(script_path)
        self.page_ctx.added_scripts.add(name)
        self.page_ctx.root.head.append(e)

    def append_inline_javascript_body_once(self, key: str, script_body: str):
        if key in self.page_ctx.added_scripts:
            return
        e = bs4.Tag(name="script")
        e.string = script_body
        self.page_ctx.added_scripts.add(key)
        self.page_ctx.root.body.append(e)

    def append_component_javascript(self, component_data: ComponentData, script_body: str):
        self.page_ctx.required_component_scripts[component_data] = script_body

    # private
    def _process_css_file(self, styles, level_up):
        def sub(path):
            if path.startswith("~"):
                asset = self._process_asset_path(path)
                if asset.type in (AssetType.Inline, AssetType.External, AssetType.LocalUrl):
                    return asset.url
                elif asset.type == AssetType.Local:
                    if level_up:
                        return "../" + asset.url
                    else:
                        return asset.url
                else:
                    assert False
            else:
                return path

        return process_css_file(styles, sub)

    def _create_styles(self):
        global_styles_dir = self._get_site_file_path("styles")
        styles_paths = [os.path.join(global_styles_dir, x) for x in ("styles.scss", "style.scss")]
        styles_paths = [x for x in styles_paths if os.path.exists(x)]

        all_styles = []

        if len(styles_paths) > 0:
            with logger_done("compiling global styles"):
                styles = sass.compile(filename=styles_paths[0])
                all_styles.append(styles)

        for c in self._component_injector.get_all_components():
            if c.compiled_styles is not None:
                all_styles.append(c.compiled_styles)

        all_styles_first_level = self._process_css_file("\n".join(all_styles), level_up=False)
        all_styles_for_static = self._process_css_file("\n".join(all_styles), level_up=True)

        if self._get_option("minify_css"):
            all_styles_for_static = optimize_css(all_styles_for_static)
        elif self._get_option("prettify_css"):
            all_styles_for_static = prettify_css(all_styles_for_static)

        self.global_styles_url = self.create_asset("styles.css", all_styles_for_static).url
        self.global_styles_content_first_level = all_styles_first_level

    def _add_global_styles(self):
        add_global_styles = self._get_option("add_global_styles")

        if add_global_styles == "inline":
            e = bs4.Tag(name="style")
            e.string = self.global_styles_content_first_level
            self.page_ctx.root.head.append(e)
        elif add_global_styles == "body":
            e = bs4.Tag(name="link", attrs=dict(rel="stylesheet", href=self.global_styles_url))
            self.page_ctx.root.body.append(e)
        elif add_global_styles == "defer":
            e = bs4.Tag(name="noscript", attrs=dict(id="deferred-styles"))
            e.append(bs4.Tag(name="link", attrs=dict(rel="stylesheet", href=self.global_styles_url)))
            self.append_inline_asset_javascript_head("defer_styles")
            self.page_ctx.root.head.append(e)
        elif add_global_styles == "head":
            e = bs4.Tag(name="link", attrs=dict(rel="stylesheet", href=self.global_styles_url))
            self.page_ctx.root.head.append(e)
        else:
            raise ValueError("unknown option for add_global_styles: {}".format(add_global_styles))

    def _add_page_styles(self, styles: str):
        e = bs4.Tag(name="style")
        e.string = styles
        self.page_ctx.root.head.append(e)

    def _get_site_file_path(self, path: str):
        return os.path.join(self.site_dir, path)

    def _get_output_file_path(self, path: str):
        return os.path.join(self.output_dir, path)

    def _read_site_file(self, path: str):
        with open(self._get_site_file_path(path), "rt") as f:
            return f.read()

    def read_site_file(self, path: str):
        return self._read_site_file(path)

    def read_site_yaml(self, path: str):
        return yaml.load(self._read_site_file(path))

    def _write_output_file(self, path: str, content: Union[bytes, str]):
        if isinstance(content, str):
            with open(self._get_output_file_path(path), "wt") as f:
                f.write(content)
        elif isinstance(content, bytes):
            with open(self._get_output_file_path(path), "wb") as f:
                f.write(content)
        else:
            raise Exception("Invalid content type")

    def _prepare(self):
        with logger_done("loading components"):
            self._component_injector.load_components()

        with logger_done("generating fonts"):
            self._generate_fonts()

        with logger_done("generating scripts"):
            self._generate_scripts()

        with logger_done("creating styles"):
            self._create_styles()

    def generate_from_dir(self, site_dir: str, output_dir: str, configuration_name: str, limit_paths=None):
        import lib.utils
        lib.utils.cache_dir = os.path.join(site_dir, ".cache")

        self.set_site_dir(site_dir)
        self.set_output_dir(output_dir)
        self.set_configuration(configuration_name)

        self.pages = []
        pages = yaml.load(self._read_site_file("pages.yaml"))["pages"]
        for page_cfg in pages:
            page = PageDescription.from_config(page_cfg)
            if limit_paths is not None and page.path not in limit_paths:
                continue
            self.pages.append(page)

        self.generate()

    def _reset(self):
        self.fonts = []
        self.page_fonts_preload_link = []
        self.page_fonts_preload_inline = []
        self._component_injector.reset()
        self.global_styles_content_first_level = None
        self.global_styles_url = None
        self.global_external_scripts = []
        self.global_preloaded_scripts = ""

    def generate(self):
        c = yaml.load(self._read_site_file("site.yaml"))
        self.options.update(c["options"])

        site_data = c.get("data", {})
        config_data = c["options"][self.configuration_name].get("data", {})
        merge_dict(site_data, config_data)

        self.set_site_data(site_data)

        os.makedirs(self.output_dir, exist_ok=True)
        os.makedirs(os.path.join(self.output_dir, "static"), exist_ok=True)

        self._reset()
        self._prepare()

        sitemap_root = bs4.BeautifulSoup(features='xml')
        self.sitemap_urlset = sitemap_root.new_tag("urlset", xmlns="http://www.sitemaps.org/schemas/sitemap/0.9")
        sitemap_root.append(self.sitemap_urlset)

        for meta in self.pages:
            with logger_done_multiline("generating page {}".format(meta.path)):
                self._generate_page(meta)

                url = sitemap_root.new_tag("url")
                loc = sitemap_root.new_tag("loc")
                loc.string = urljoin(self._get_option("base_url"), meta.path.lstrip("/"))
                url.append(loc)
                self.sitemap_urlset.append(url)

        sitemap_content = sitemap_root.prettify(formatter=None)
        if self._get_option("minify_html"):
            sitemap_content = html_minifier(sitemap_content)
        self._write_output_file("sitemap.xml", sitemap_content)

        index_path = self._get_option("generate_index")  # type: str
        if index_path is not None:
            index_content = ""
            for page in self.pages:
                index_content += "<a href='{}'>{}</a><br/>".format(page.url, page.url)

            self._write_output_file(index_path, index_content)

    def _generate_page(self, page: PageDescription):
        main_path = os.path.join(self.site_dir, "main.html")
        root = bs(open(main_path, "rt").read(), "html5lib")

        self.page_ctx = PageContext(page, root)

        lvl = page.get_level()

        title = page.title or self._get_option("title")
        if title is not None:
            root.head.insert(0, new_tag("title", html=title))

        description = page.description or self._get_option("description")
        if description is not None:
            root.head.insert(0, new_tag("meta", name="description", content=description))

        keywords = self._get_option("keywords")
        if keywords is not None:
            root.head.insert(0, new_tag("meta", name="keywords", content=keywords))

        root.head.insert(0, new_tag("base", href="/.." * (lvl - 1)))
        robots_content = []
        if page.no_index: robots_content.append("noindex")
        if page.no_follow: robots_content.append("nofollow")
        if len(robots_content) > 0:
            root.head.insert(0, new_tag("meta", name="robots", content=", ".join(robots_content)))
        root.head.insert(0, new_tag("meta", charset="utf-8"))

        # add styles
        # if self._get_option("add_global_styles") is True:
        #     self._add_global_styles()
        # if self._get_option("add_global_styles") == "inline":
        #     self._add_global_styles(inline=True)
        self._add_global_styles()

        # add global scripts
        e = bs4.Tag(name="script")
        e.string = self._process_js(
                "var RESTATIC_CONFIGURATION = '{}';\n".format(self.configuration_name))
        root.head.append(e)

        self._add_scripts()

        # inject components
        self._component_injector.inject(self.page_ctx, root)

        # add component scripts
        script_path = os.path.join(script_dir, "../scripts", "components.js")
        component_js_code = read_file(script_path)
        for component_data in self._component_injector.components:
            script_body = component_data.get_script()

            class_name = "{name}Component".format(name=component_data.name_camelcase)
            if script_body is None or "class {class_name}".format(class_name=class_name) not in script_body:
                continue

            component_js_code += """
// *******************************
//        Component: {name}
// *******************************
{script_body}
window.addEventListener('load', function () {{
    let inst = new {name}Component();
    inst.setup("{content_id}", "{host_id}");
    inst.install();
}});
""".format(content_id=component_data.content_id,
           host_id=component_data.host_id,
           name=component_data.name_camelcase,
           script_body=script_body)

        e = bs4.Tag(name="script", attrs=dict(
                src=self.create_asset("main.js", self._process_js(component_js_code)).url
        ))
        root.body.append(e)

        self._process_images(page, root)

        page_inline_styles = ""
        if len(self.page_fonts_preload_link) > 0:
            for f in self.page_fonts_preload_link:
                for v in f.variants:
                    root.head.append(
                            bs4.Tag(name="link",
                                    attrs={"as": "font", "href": v.url, "rel": "preload", "crossorigin": None}))

            font_styles = "\n".join(x.get_css_urls() for x in self.page_fonts_preload_link)
            page_inline_styles += font_styles
            self._add_page_styles(font_styles)

        if len(self.page_fonts_preload_inline) > 0:
            font_styles = "\n".join(x.get_css_inline() for x in self.page_fonts_preload_inline)
            page_inline_styles += font_styles
            self._add_page_styles(font_styles)

        if self._get_option("per_page_inline_styles"):
            page_styles = self.global_styles_content_first_level
            page_inline_styles += page_styles
            page_inline_styles = uncss(root, page_inline_styles)

        if page_inline_styles != "":
            if self._get_option("minify_css"):
                page_inline_styles = optimize_css(page_inline_styles)
            self._add_page_styles(page_inline_styles)

        self._page_tree_postprocess(root)

        if self._get_option("prettify_html"):
            html = root.prettify(formatter=None)
        else:
            html = root.encode("utf-8", formatter=None).decode("utf-8")

        output_path = os.path.join(self.output_dir, page.get_output_path())
        os.makedirs(os.path.dirname(output_path), exist_ok=True)

        html = self._page_raw_postprocess(html)

        if self._get_option("minify_html"):
            html = html_minifier(html)

        write_file(output_path, html)

        return html

    def _page_tree_postprocess(self, root):
        for element in list(root.descendants):
            if element.name == "script":
                pass

            # if isinstance(element, bs4.Comment):
            #     element.extract()

    def _page_raw_postprocess(self, html):
        assets_map = self._get_option("assets_map", [])
        for asset in assets_map:
            name, path = asset.split(":", 1)
            repl = self._process_asset_path(path)

            html = html.replace("%%%{}%%%".format(name), repl)

        return html

    def find_asset(self, path: str):
        if path is None:
            return None

        if len(path) == 0 or path[0] != "~":
            return None

        m = re.match("^([^()]*)(\([^()]*\))?$", path)
        if m:
            path = m.group(1)
            path_prop = m.group(2) or ""
        else:
            path_prop = ""

        fs_path = os.path.join(self.site_dir, path[1:])
        if not os.path.exists(fs_path):
            raise ValueError("Unable to find asset {}".format(path))

        return fs_path + path_prop

    def create_asset(self, name: str, content) -> Asset:
        fhash = compute_string_hash(content)
        if not self._get_option("hash_assets", True):
            fhash = "statichash"
        name, ext = os.path.splitext(name)
        name = "{}.{}{}".format(name, fhash, ext)

        dst_fs_path = os.path.join(self.output_dir, "static", name)
        write_file(dst_fs_path, content)

        static_url = self._get_option("static_url")
        if static_url:
            url = urljoin(static_url, name)
            return Asset(AssetType.LocalUrl, url, local_path=dst_fs_path)
        else:
            url = urljoin("static/", name)
            return Asset(AssetType.Local, url, local_path=dst_fs_path)

    def create_asset_from_file(self, name: str, fs_path: str, binary=False) -> Asset:
        return self.create_asset(name, read_binary_file(fs_path) if binary else read_file(fs_path))

    def _process_asset_path(self, path: str) -> Asset:
        fs_path = self.find_asset(path)
        if fs_path is None:
            return Asset(AssetType.External, path)

        if self.ic.is_image(fs_path):
            return self.ic.get_image_with_prop(fs_path)

        file_name = os.path.basename(fs_path)
        _, file_ext = os.path.splitext(file_name)

        if file_ext.lower() in (".js",):
            js_src = read_file(fs_path)
            js_src = self._process_js(js_src)
            return self.create_asset(file_name, js_src)
        else:
            return self.create_asset_from_file(file_name, fs_path)

    def _process_image_tag(self, image_element: bs4.Tag):
        path = image_element.attrs["src"]
        if len(path) == 0 or path[0] != "~":
            return

        path = path[1:]

        ip = ImageProperties.parse(path)
        fs_path = self._get_site_file_path(ip.path)

        tag_width = image_element.attrs.get("width")
        tag_height = image_element.attrs.get("height")

        if ip.has_size():
            width = ip.width
            height = ip.height
        else:
            width = int(tag_width) if tag_width is not None else None
            height = int(tag_height) if tag_height is not None else None

        image = self.ic.get_image(fs_path, width, height, ip.format)

        image_element.attrs["src"] = image.url

    def _process_images(self, page: PageDescription, root):
        def check_attr(element, name):
            if name in element.attrs:
                asset = self._process_asset_path(element.attrs[name])
                element.attrs[name] = asset.url

        for element in root.descendants:
            if hasattr(element, "attrs") and "_re_skip" in element.attrs:
                del element.attrs["_re_skip"]
                continue

            if hasattr(element, "attrs"):
                style = element.attrs.get("style")
                if style is not None:
                    style = self._process_css_file(style, level_up=False)  # html inline styles, no need to level up
                    element.attrs["style"] = style

            if element.name == "img":
                self._process_image_tag(element)

            if element.name == "link":
                check_attr(element, "href")

            if element.name == "a":
                if "href" in element.attrs:
                    href = element.attrs["href"]
                    if href.startswith("/"):
                        # TODO: check if page exists
                        element.attrs["href"] = page.get_to_root() + href

            if element.name == "script":
                check_attr(element, "src")

                if element.attrs.get("type") not in ("application/ld+json",):
                    if element.string is not None:
                        element.string = self._process_js(element.string)

    def _process_js(self, js_src):
        js_src = babelify(js_src,
                          babelify=self._get_option("babelify_js"),
                          minify=self._get_option("minify_js"))
        return js_src

    def _get_option(self, name: str, default=None):
        v = self.options[self.configuration_name].get(name)

        if v is not None:
            return v
        else:
            v = self.options.get(name)
            if v is not None:
                return v
            else:
                return default

    def _generate_scripts(self):
        pass
        # scripts = self._get_option("scripts")
        #
        # if not isinstance(scripts, list):
        #     return
        #
        # for script in scripts:
        #     url = script["url"]
        #     preload = script.get("preload", False)
        #     async = script.get("async", False)
        #     defer = script.get("defer", False)
        #
        #     path = self.find_asset(url)
        #     if path is None:
        #         if preload:
        #             content = load_url_text(url)
        #             self.global_preloaded_scripts += content + "\n"
        #         else:
        #             self.global_external_scripts.append({"url": url, "async": async, "defer": defer})
        #     else:
        #         content = read_file(path)
        #         self.global_preloaded_scripts += content + "\n"

    def _generate_fonts(self):
        fonts = self._get_option("fonts")

        if not isinstance(fonts, list):
            return

        for font in fonts:
            css_url = font["url"]
            preload = font.get("preload", False)
            display = font.get("display")

            # if not self._get_option("per_page_inline_styles"):
            #    preload = False

            css_content = load_cache(css_url)
            if css_content is None:
                css_content = requests.get(css_url).text
                save_cache(css_url, css_content)

            font_data = FontData()

            def r(url):
                font_content = load_cache(url)
                if font_content is None:
                    font_content = requests.get(urljoin(css_url, url)).content
                    save_cache(url, font_content)

                url_hash = compute_string_hash(url)
                name, ext = os.path.splitext(urlparse(url).path)
                # print("URL", url_hash, url, name, ext, len(font_data))

                mime_map = {
                    ".ttf": "application/x-font-ttf",
                    ".woff": "application/x-font-woff",
                    ".woff2": "application/x-font-woff2",
                    ".eot": "application/vnd.ms-fontobject",
                    ".svg": "image/svg+xml",
                }

                if len(ext) == 0:
                    ext = ".ttf"

                mime = mime_map.get(ext)
                if mime is None:
                    raise Exception("Unsupported font extension: {}".format(ext))

                v = FontVariantData()
                v.file_name = "font.{hash}{ext}".format(hash=url_hash, ext=ext)
                v.mime = mime
                v.content = font_content
                v.url = self.create_asset(v.file_name, v.content).url
                font_data.variants.append(v)
                return v.file_name

            font_data.css_content = process_css_file(css_content, r)

            if display is not None:
                font_data.css_content = font_data.css_content.replace("@font-face {",
                                                                      "@font-face {{ font-display: {};".format(display))

            if preload == "inline":
                self.page_fonts_preload_inline.append(font_data)
            elif preload == "link":
                self.page_fonts_preload_link.append(font_data)
            else:
                self.fonts.append(font_data)

    def _add_scripts(self):

        scripts = self._get_option("scripts")

        if not isinstance(scripts, list):
            return

        idx = 1
        preloaded_js = ""

        def emit_preloaded():
            nonlocal preloaded_js, idx

            if len(preloaded_js) > 0:
                e = bs4.Tag(name="script", attrs=dict(
                        src=self.create_asset("script{}.js".format(idx), self._process_js(preloaded_js)).url
                ))
                self.page_ctx.root.body.append(e)
                idx += 1
                preloaded_js = ""

        for script in scripts:
            url = script["url"]
            preload = script.get("preload", False)
            async_load = script.get("async", False)
            defer = script.get("defer", False)

            path = self.find_asset(url)
            if path is None:
                if preload:
                    content = load_url_text(url)
                    preloaded_js += content + "\n"
                else:
                    emit_preloaded()

                    e = bs4.Tag(name="script", attrs=dict(src=url))
                    if async_load:
                        e["async"] = None
                    if defer:
                        e["defer"] = None
                    self.page_ctx.root.body.append(e)
                    # self.global_external_scripts.append({"url": url, "async": async, "defer": defer})
            else:
                content = read_file(path)
                preloaded_js += content + "\n"

        emit_preloaded()
