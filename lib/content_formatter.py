import re
import html

import shortuuid as shortuuid

from lib.tag_options import TagOptions

opts_map = [
    ('-(\d+)', 'colspan="\1"'),
    ('\|(\d+)', 'rowspan="\1"'),
    ('l', 'align="left"'),
    ('c', 'align="center"'),
    ('r', 'align="right"'),
    ('t', 'valign="top"'),
    ('m', 'valign="middle"'),
    ('b', 'valign="bottom"'),
]


def parse_list_opts(x):
    opts = []
    for pattern, repl in opts_map:
        m = re.search(pattern, x)
        if m:
            x = re.sub(pattern, "", x)
            gr = m.groups()
            if len(gr) >= 1:
                repl = repl.replace("\1", m.group(1))
            opts.append(repl)
    return " ".join(opts)


def process_paragraph(text):
    opened_tags = []
    out_lines = []
    list_started = False
    line_started = False
    table_started = False
    row_started = False

    def end_list():
        nonlocal out_lines, list_started, line_started, table_started, opened_tags
        if list_started and line_started:
            out_lines.append("</span></li>")

        while len(opened_tags) > 0:
            out_lines.append("</{0}>".format(opened_tags.pop()))

        list_started = False
        line_started = False

    def end():
        nonlocal out_lines, list_started, line_started, table_started, opened_tags

        end_list()

        if table_started:
            if line_started:
                out_lines.append("</{0}>".format(opened_tags.pop()))
            if row_started:
                out_lines.append("</tr>")
            out_lines.append("</table>")

        table_started = False

    for line in text.splitlines():
        list_match = re.match("^([#*]+) ", line)
        table_match = re.match("^([%$])([^ ]*)?", line)
        # LISTS
        if list_match:
            if table_started and line_started and not list_started:
                line_started = False
            list_started = True
            stars = list_match.group(1)
            level = len(stars)
            last_star = stars[-1]
            tag = None
            if last_star == "*":
                tag = "ul"
            elif last_star == "#":
                tag = "ol"
            cont = list_match.string[list_match.end(0):]
            # print("LINE", line, stars, level, last_star, tag, cont)

            while level < len(opened_tags):
                tag = opened_tags.pop()
                if line_started:
                    out_lines.append("</span></li>")
                out_lines.append("</{0}>".format(tag))
                line_started = False

            if level > len(opened_tags):
                if line_started:
                    out_lines.append("</span></li>")

                opened_tags.append(tag)
                out_lines.append("<{0}>".format(tag))
                out_lines.append("<li><span>")
                out_lines.append(cont)
                line_started = True
            else:
                if line_started:
                    out_lines.append("</span></li>")
                out_lines.append("<li><span>")
                out_lines.append(cont)
                line_started = True
        elif line == "%%%%%":
            end_list()
            table_started = True
            out_lines.append('<table class="mytable">')
        # TABLES
        elif table_started and table_match:
            end_list()
            c = table_match.group(1)
            opts = table_match.group(2)
            cont = table_match.string[table_match.end(0):]
            # print("W", c, opts)
            if c == "%":
                tag = "th"
            elif c == "$":
                tag = "td"
            if c == "%" or c == "$":
                if line_started:
                    out_lines.append("</{0}>".format(opened_tags.pop()))
                if not row_started:
                    out_lines.append("<tr>")
                    row_started = True
                out_lines.append("<{0} {1}>".format(tag, parse_list_opts(opts)))
                opened_tags.append(tag)
                out_lines.append(cont)
                line_started = True
        elif table_started and line == "-":  # table cell separator
            end_list()
            if line_started:
                out_lines.append("</th>")
            if row_started:
                out_lines.append("</tr>")
                row_started = False
                # print("W", "-")
        elif len(line) == 0:
            end()
        # FREE LINES
        else:
            out_lines.append(line)

    end()

    return "\n".join(out_lines)


def parse_cmd(x):
    x = x.split(" ", 1)
    if len(x) == 1:
        return x[0], {}
    else:
        cmd, options_str = x
        opts = {}
        for cmd_str in options_str.split(","):
            p = cmd_str.split("=", 2)
            if len(p) == 1:
                key, val = p[0], True
            else:
                key, val = p
            opts[key] = val
        return cmd, opts


def parse_title(x):
    img_desc = x.split("|", 2)
    if len(img_desc) == 1:
        return img_desc[0], img_desc[0]
    elif len(img_desc) == 2:
        return img_desc[0], img_desc[1]
    return None, None


def get_open_tag_re(tag):
    # match [tag], [tag option1=value]
    return re.compile("\[{0}([= ][^\]*]*)?\]".format(re.escape(tag)))


def get_close_tag_re(tag):
    return re.compile("\[\/{0}\]".format(re.escape(tag)))


def get_simple_tag_re(tag, parts):
    # match [tag](part1)(part2)
    # match [tag option1=value](part1)(part2)
    regex = "(\[{0}(?:[= ][^\]*]*)?\])".format(re.escape(tag))
    regex += "\(([^\)]+)\)" * parts
    return re.compile(regex)


class Formatter:
    callback = None
    tag = None
    parts = None  # for simple tag
    line_start = None  # for inline


class FormatterContext:
    fmt = None  # type: Formatter
    options = None  # type: TagOptions
    text = None  # type:str

    def print(self):
        print("options", self.options.main, self.options.opts)


class ContentFormatter:
    range_formatters = None
    simple_formatters = None
    inline_formatters = None
    formatters_stack = None
    replace_parts = None

    def __init__(self):
        self.range_formatters = []
        self.simple_formatters = []
        self.inline_formatters = []
        self.formatters_stack = []
        self.replace_parts = {}

    def register_range_tag(self, tag, callback):
        f = Formatter()
        f.tag = tag
        f.callback = callback
        self.range_formatters.append(f)

    def register_simple_tag(self, tag, parts, callback):
        f = Formatter()
        f.tag = tag
        f.parts = parts
        f.callback = callback
        self.simple_formatters.append(f)

    def register_inline(self, tag, callback, line_start=False):
        f = Formatter()
        f.tag = tag
        f.line_start = line_start
        f.callback = callback
        self.inline_formatters.append(f)

    def add_replace(self, value):
        key = shortuuid.uuid()
        self.replace_parts[key] = value
        return key

    def process_range(self, ctx):
        text = ctx.text
        text = ctx.fmt.callback(self, self.formatters_stack, ctx, text)
        # text = self._format_inner(text)
        return text

    def process_range_formatter(self, fmt: Formatter, text: str):
        open_tag_re = get_open_tag_re(fmt.tag)
        close_tag_re = get_close_tag_re(fmt.tag)

        pos = 0

        tag_start = None
        opened_cnt = 0

        while pos < len(text):
            ms = open_tag_re.search(text, pos)
            me = close_tag_re.search(text, pos)

            ms_pos = ms.start(0) if ms else len(text) + 1
            me_pos = me.start(0) if me else len(text) + 1
            # print("ms", ms_pos, "me", me_pos)

            if ms_pos < me_pos:  # got open tag
                opened_cnt += 1
                if opened_cnt == 1:
                    tag_start = ms
                    # print("range_start", range_start)
                pos = ms.end(0)
            elif me_pos < ms_pos:  # got close tag
                opened_cnt -= 1
                if opened_cnt == 0:
                    range_start = tag_start.end(0)
                    outer_start = tag_start.start(0)
                    range_end = me_pos
                    outer_end = me.end(0)

                    range_text = text[range_start:range_end]
                    ctx = FormatterContext()
                    ctx.fmt = fmt
                    ctx.options = TagOptions.parse(tag_start.group(0))
                    ctx.text = range_text
                    self.formatters_stack.append(ctx)

                    id = self.add_replace(self.process_range(ctx))
                    text = text[0:outer_start] + id + text[outer_end:]
                    pos = outer_start + len(id)

                    self.formatters_stack.pop()
                else:
                    pos = me.end(0)
            else:
                break

        # print()

        return text

    def process_simple_formatter(self, fmt: Formatter, text: str):
        tag_re = get_simple_tag_re(fmt.tag, fmt.parts)

        def repl(m):
            parts = []

            for i in range(fmt.parts):
                parts.append(m.group(i + 2))

            options = m.group(1)

            ctx = FormatterContext()
            ctx.fmt = fmt
            ctx.options = TagOptions.parse(options)
            self.formatters_stack.append(fmt)
            txt = fmt.callback(self, self.formatters_stack, ctx, *parts)
            self.formatters_stack.pop()
            return txt

        text = tag_re.sub(repl, text)
        return text

    def process_inline_formatter(self, fmt: Formatter, text: str):
        regex = ""
        if fmt.line_start:
            regex += "^"
        regex += "{0}([^\n]+?){0}".format(re.escape(fmt.tag))
        tag_re = re.compile(regex, flags=re.MULTILINE)

        self.formatters_stack.append(fmt)

        def repl(m):
            return fmt.callback(self, self.formatters_stack, fmt, m.group(1))

        text = tag_re.sub(repl, text)
        self.formatters_stack.pop()
        return text

    def format(self, text):
        text = text.strip()

        # text = text.replace("\n\n", '\n<div class="sep"></div>\n')
        text = text.replace("\[", "\1")
        text = text.replace("\]", "\2")
        text = text.replace("\(", "\3")
        text = text.replace("\)", "\4")

        text = self._format_inner(text)

        text = text.replace("\1", "[")
        text = text.replace("\2", "]")
        text = text.replace("\3", "(")
        text = text.replace("\4", ")")
        text = text.replace("\5", "\n")

        while True:
            replaced = False
            for k, v in self.replace_parts.items():
                if k in text:
                    replaced = True
                    text = text.replace(k, v)
            if not replaced:
                break

        return text

    def _format_inner(self, text):
        for fmt in self.range_formatters:
            text = self.process_range_formatter(fmt, text)
        for fmt in self.simple_formatters:
            text = self.process_simple_formatter(fmt, text)
        for fmt in self.inline_formatters:
            text = self.process_inline_formatter(fmt, text)

        text = process_paragraph(text)

        # paragraphs = map(process_paragraph, text.split("\0"))
        # text = '\n'.join('<div class="p">\n{0}\n</div>'.format(p) for p in paragraphs)
        # text = '\n'.join(paragraphs)

        text = text.replace(r"\\", "<br/>")
        return text

    @staticmethod
    def create_default():
        content_formatter = ContentFormatter()

        def process_href(x):
            return x

        def fmt_link1(content_formatter, stack, fmt: FormatterContext, p1):
            rel = ""

            prv = fmt.options.get_opt('private', False)
            if prv:
                rel += "noreferrer "
            prv = fmt.options.get_opt('nofollow', False)
            if prv:
                rel += "nofollow "

            href = process_href(p1)
            return '<a href="{0}" rel="{rel}">{0}</a>'.format(html.escape(href), rel=rel)

        def fmt_link2(content_formatter, stack, fmt: FormatterContext, p1, p2):
            rel = ""

            prv = fmt.options.get_opt('private', False)
            if prv:
                rel += "noreferrer "
            prv = fmt.options.get_opt('nofollow', False)
            if prv:
                rel += "nofollow "

            if len(p1) == 0:
                href = process_href(p1)
                return '<a href="{0}" rel="{rel}">{0}</a>'.format(html.escape(href), rel=rel)
            else:
                href = process_href(p1)
                return '<a href="{0}" rel="{rel}">{1}</a>'.format(html.escape(href), html.escape(p2), rel=rel)

        def fmt_p(content_formatter, stack, fmt: FormatterContext, cnt):
            cls = fmt.options.get_opt('class', "")
            return '<p class="{1}">{0}</p>'.format(content_formatter._format_inner(cnt), cls)

        def fmt_img2(content_formatter, stack, fmt: FormatterContext, cnt, cnt2):
            return '<img src="{0}" alt="{1}" />'.format(html.escape(cnt), html.escape(cnt2))

        def fmt_img1(content_formatter, stack, fmt: FormatterContext, cnt):
            return '<img src="{0}" />'.format(html.escape(cnt))

        content_formatter.register_range_tag("p", fmt_p)
        content_formatter.register_inline("***", lambda c, s, f, t: "<b>{0}</b>".format(html.escape(t)))
        content_formatter.register_inline("**", lambda c, s, f, t: "<i>{0}</i>".format(html.escape(t)))
        content_formatter.register_inline("=====", lambda c, s, f, t: "<h5>{0}</h5>".format(html.escape(t)),
                                          line_start=True)
        content_formatter.register_inline("====", lambda c, s, f, t: "<h4>{0}</h4>".format(html.escape(t)),
                                          line_start=True)
        content_formatter.register_inline("===", lambda c, s, f, t: "<h3>{0}</h3>".format(html.escape(t)),
                                          line_start=True)
        content_formatter.register_inline("==", lambda c, s, f, t: "<h2>{0}</h2>".format(html.escape(t)),
                                          line_start=True)
        content_formatter.register_inline("=", lambda c, s, f, t: "<h1>{0}</h1>".format(html.escape(t)),
                                          line_start=True)
        content_formatter.register_simple_tag("link", 2, fmt_link2)
        content_formatter.register_simple_tag("link", 1, fmt_link1)

        content_formatter.register_simple_tag("img", 2, fmt_img2)
        content_formatter.register_simple_tag("img", 1, fmt_img1)
        return content_formatter
