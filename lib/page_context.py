from typing import List, Dict

import bs4

from lib.component_data import ComponentData
from lib.page_meta import PageDescription


class PageContext:
    def __init__(self, meta: PageDescription, root: bs4.BeautifulSoup):
        self.meta = meta
        self.root = root

        self.added_scripts = set()
        self.used_components = set()

        self.required_component_scripts: Dict[ComponentData,str] = {}
