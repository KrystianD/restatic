from typing import List, Dict, Any


class RouterEntry:
    def __init__(self, component_name: str, component_data: Any):
        self.component_name = component_name
        self.component_data = component_data


class PageDescription:
    def __init__(self):
        self._path: str = None
        self.title: str = None
        self.description: str = None
        self.no_index: bool = None
        self.no_follow: bool = None

        self.page_data: Any = {}

        self._router_entries: List[RouterEntry] = []
        self._level = None
        self._output_path = None

    @property
    def url(self) -> str:
        return self._path.lstrip("/")

    @property
    def path(self) -> str:
        return self._path

    @path.setter
    def path(self, path: str):
        self._path = path

        assert self._path[0] == "/"
        assert ".." not in self._path

        name = "/" + self._path.lstrip("/")
        is_index = len(name) == 0 or name[-1] == "/"
        if is_index:
            name += "index"
        self._level = name.count("/")

        if ".html" in name:
            self._output_path = name.lstrip("/")
        else:
            self._output_path = name.lstrip("/") + ".html"

    @property
    def components(self) -> List[RouterEntry]:
        return self._router_entries

    def add_router_entry(self, component_name: str, router_path_data: Any):
        self._router_entries.append(RouterEntry(component_name, router_path_data))

    def set_router_entries(self, router_entries: List[RouterEntry]):
        self._router_entries = router_entries

    def get_output_path(self):
        return self._output_path

    def get_level(self):
        return self._level

    def get_to_root(self):
        return ("../" * (self.get_level() - 1)).rstrip("/")

    @staticmethod
    def from_config(data: Dict[str, Any]):
        meta = PageDescription()
        meta.path: str = data["path"]
        meta.title: str = data.get("title")
        meta.description: str = data.get("description")
        meta.page_data = data.get("data", {})
        meta.no_index: bool = data.get("noindex") is True
        meta.no_follow: bool = data.get("nofollow") is True

        components = data.get("components")
        if components is not None:
            if isinstance(components, str):
                components = [x.strip() for x in data["components"].split(",")]
            else:
                components = data["components"]

            for c in components:
                meta.add_router_entry(c, {})

        # if meta.components[0].lower() == "root":
        #    meta.components = meta.components[1:]

        return meta
