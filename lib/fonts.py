from lib.utils import base64_encode, process_css_file


class FontsGenerator:
    def __init__(self):
        pass

    def _generate_fonts(self):
        fonts = self._get_option("fonts")

        for font in fonts:
            if "googleapis" in font:
                css_content = requests.get(font)
                print(css_content.text)

        print("ASD", fonts)


class FontVariantData:
    def __init__(self):
        self.file_name: str = None
        self.mime: str = None
        self.content: bytes = None
        self.url: str = None


class FontData:
    def __init__(self):
        self.css_content = None  # type: str
        self.variants = []  # type: List[FontVariantData]

    def get_css_inline(self):
        def r(url):
            variant = [x for x in self.variants if x.file_name == url][0]
            return base64_encode(variant.mime, variant.content)

        return process_css_file(self.css_content, r)

    def get_css_urls(self):
        def r(url):
            variant = [x for x in self.variants if x.file_name == url][0]
            return variant.url

        return process_css_file(self.css_content, r)
