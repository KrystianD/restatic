from typing import Dict, Any

from lib.context import Context


class Component:
    def __init__(self, ctx: Context, replace_tag=False):
        self.ctx = ctx
        self.host_attr = {}
        self.host_classes = set()
        self.replace_tag = replace_tag

    def set_input(self, params: Dict[str, Any]):
        for k, v in params.items():
            setattr(self, k, v)

    def set_var(self, name: str, value: Any):
        setattr(self, name, value)

    def process(self):
        pass

    def get_inner_content(self):
        return self.ctx.inner_content

    """
    def get_html(self):
        path = inspect.getfile(self.__class__)

        component_dir = os.path.dirname(path)

        tag = self.__class__.get_tag_name()
        tag = camelcase2underscore(tag)
        html_path = os.path.join(component_dir, tag + ".html")

        with open(html_path, "rt") as f:
            cnt = f.read()

        cnt = "<component_root>{}</component_root>".format(cnt)

        return cnt

    @classmethod
    def get_styles(cls):
        path = inspect.getfile(cls)

        component_dir = os.path.dirname(path)

        tag = cls.get_tag_name()
        tag = camelcase2underscore(tag)
        html_path = os.path.join(component_dir, tag + ".scss")

        with open(html_path, "rt") as f:
            return f.read()

    @classmethod
    def _get_tag_name_internal(cls):
        tag = cls.get_tag_name()
        return tag.lower()
    """


class DefaultComponent(Component):
    def __init__(self, ctx: Context):
        super().__init__(ctx)
