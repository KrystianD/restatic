import base64
import hashlib
import imp
import json
import os
import pickle
import re
import subprocess
import sys
import tempfile
from contextlib import contextmanager
from typing import Union, Callable
import copy

import bs4
import requests

script_dir = os.path.dirname(os.path.realpath(__file__))
_hash_cache = {}

cache_dir: str = None


def camelcase2underscore(x):
    def fn(m):
        txt = m.group(0).lower()
        if m.end(0) == len(m.string):  # split group of uppercase letters
            return "_" + txt
        else:
            return "_" + (txt[:-1] + "_" + txt[-1]).strip("_")

    return re.sub("[A-Z]+", fn, x).lstrip("_")


def underscore2camelcase(x):
    return x.replace("_", " ").title().replace(" ", "")


def merge_dict(d1, d2):
    for k in d2:
        if k in d1 and isinstance(d1[k], dict) and isinstance(d2[k], dict):
            merge_dict(d1[k], d2[k])
        else:
            d1[k] = d2[k]


def parse_toplevel(prelude, selector_name, host_selector):
    from tinycss2 import ast

    new_prelude = []

    it = iter(prelude)
    added = False
    skip_add_selector = False
    while True:
        try:
            prelude_node = next(it)

            if isinstance(prelude_node, ast.LiteralToken) and prelude_node.value == ",":
                skip_add_selector = False
            if isinstance(prelude_node, ast.WhitespaceToken):
                added = False

            if added or skip_add_selector:
                new_prelude.append(prelude_node)
                continue

            if isinstance(prelude_node, ast.LiteralToken) and prelude_node.value == ":":
                prelude_node2 = next(it)
                if isinstance(prelude_node2, ast.IdentToken) and prelude_node2.value == "host":
                    new_prelude.append(ast.SquareBracketsBlock(0, 0, [ast.IdentToken(0, 0, host_selector)]))
                    added = True
                elif isinstance(prelude_node2, ast.IdentToken) and prelude_node2.value == "deep":
                    added = True
                    skip_add_selector = True
                else:
                    new_prelude.append(prelude_node)
                    new_prelude.append(prelude_node2)
            elif isinstance(prelude_node, ast.IdentToken):
                new_prelude.append(prelude_node)
                new_prelude.append(ast.SquareBracketsBlock(0, 0, [ast.IdentToken(0, 0, selector_name)]))
                added = True
            else:
                new_prelude.append(prelude_node)
        except StopIteration:
            break

    return new_prelude


def add_selector_to_css(style, selector_name, host_selector):
    import tinycss2
    from tinycss2 import ast

    nodes = tinycss2.parse_stylesheet(style)
    for node in nodes:
        if isinstance(node, ast.QualifiedRule):
            node.prelude = parse_toplevel(node.prelude, selector_name, host_selector)
        if isinstance(node, ast.AtRule):
            new_content = []
            selector_prelude = []

            if node.content is not None:
                for subnode in node.content:
                    if isinstance(subnode, ast.CurlyBracketsBlock):
                        selector_prelude = parse_toplevel(selector_prelude, selector_name, host_selector)

                        selector_prelude.append(subnode)
                        new_content += selector_prelude
                        selector_prelude = []
                    else:
                        selector_prelude.append(subnode)

                node.content = new_content

    css = tinycss2.serialize(nodes)
    return css


def compute_file_hash(path):
    global _hash_cache
    mtime = os.path.getmtime(path)

    key = (path, mtime)
    if key in _hash_cache:
        return _hash_cache[key]

    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(path, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    h = hasher.hexdigest()[:8]
    _hash_cache[key] = h
    return h


def compute_string_hash(content: Union[str, bytes]):
    if isinstance(content, str):
        content = content.encode("utf-8")
    return hashlib.md5(content).hexdigest()[:8]


def compute_py_hash(data):
    return compute_string_hash(json.dumps(data))


def uncss(html_root: bs4.BeautifulSoup, css):
    html_root = copy.copy(html_root)

    [s.extract() for s in html_root('script')]
    html = str(html_root)

    h = compute_py_hash((html, css))
    minified = load_cache(h)

    if minified is not None:
        return minified

    with tempfile.NamedTemporaryFile(suffix=".html", mode="wt") as html_fp:
        with tempfile.NamedTemporaryFile(suffix=".css", mode="wt") as css_fp:
            html_fp.write(html)
            html_fp.flush()
            css_fp.write(css)
            css_fp.flush()

            uncss_path = os.path.abspath(os.path.join(script_dir, "../node_modules/.bin/uncss"))
            args = [uncss_path, os.path.basename(html_fp.name), "-s", os.path.basename(css_fp.name)]

            minified = subprocess.check_output(args, cwd="/tmp", stderr=subprocess.DEVNULL).decode("utf-8")
            save_cache(h, minified)
            return minified


def prettify_css(input: str):
    return input
    # prettified = load_cache(input)
    #
    # if prettified is not None:
    #     return prettified
    #
    # save_cache(input, prettified)
    # return prettified

def optimize_css(input: str):
    minified = load_cache(input)

    if minified is not None:
        return minified

    p = subprocess.Popen(
            args=[
                os.path.join(script_dir, "../node_modules/.bin/postcss"),
            ],
            cwd=os.path.join(script_dir, ".."),
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE)

    response, status = p.communicate(input.encode("utf-8"))
    if p.returncode != 0:
        print("postcss error", status.decode("utf-8"), file=sys.stderr)
        raise Exception("postcss error")
    minified = response.decode("utf-8")

    save_cache(input, minified)
    return minified


def html_minifier(input):
    minified = load_cache(input)

    if minified is not None:
        return minified

    p = subprocess.Popen(
            args=[
                os.path.join(script_dir, "../node_modules/.bin/html-minifier"),
                "--html5",
                "--remove-comments",
                "--minify-js",
                "--remove-empty-attributes",
                "--remove-tag-whitespace",
                "--collapse-whitespace",
                "--conservative-collapse",
                "--minify-css",
            ],
            cwd=os.path.join(script_dir, ".."),
            stdin=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE)

    response, status = p.communicate(input.encode("utf-8"))
    if p.returncode != 0:
        print("html_minifier error", status.decode("utf-8"), file=sys.stderr)
        raise Exception("html-minifier error")
    minified = response.decode("utf-8")
    save_cache(input, minified)
    return minified


def babelify(js, babelify: bool, minify=False):
    key = (js, babelify, minify)
    transformed = load_cache(key)

    if transformed is not None:
        return transformed

    args = [
        os.path.join(script_dir, "../node_modules/.bin/babel"),
    ]
    if babelify:
        args.append("--presets")
        args.append("env")
    if minify:
        args.append("--minified")
        args.append("--no-comments")
        args.append("--compact")
        args.append("true")

    p = subprocess.Popen(args=args,
                         cwd=os.path.join(script_dir, ".."),
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    response, err = p.communicate(js.encode("utf-8"))
    if p.returncode != 0:
        print(response.decode("utf-8"), file=sys.stderr)
        print(err.decode("utf-8"), file=sys.stderr)
        raise Exception("babel error")
    transformed = response.decode("utf-8")

    save_cache(key, transformed)
    return transformed


def resize_with_aspect(orig_width, orig_height, rect_width, rect_height):
    if rect_width is None:
        rect_width = 99999
    if rect_height is None:
        rect_height = 99999
    aspect = orig_width / orig_height

    if rect_width < orig_width or rect_height < orig_height:
        if rect_width / rect_height > aspect:
            new_width = rect_height * aspect
            new_height = rect_height
        else:
            new_width = rect_width
            new_height = rect_width / aspect
    else:
        new_width = orig_width
        new_height = orig_height
    return int(new_width), int(new_height)


def save_cache(name, data):
    from lib.generator import Generator
    if Generator.no_cache:
        return
    name = compute_py_hash(name)
    path = os.path.join(cache_dir, name)
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, "wb") as f:
        pickle.dump(data, f)


def load_cache(name):
    from lib.generator import Generator
    if Generator.no_cache:
        return None
    name = compute_py_hash(name)
    path = os.path.join(cache_dir, name)
    try:
        with open(path, "rb") as f:
            return pickle.load(f)
    except FileNotFoundError:
        return None
    except:
        raise


def load_url_text(url):
    content = load_cache(url)
    if content is None:
        content = requests.get(url).text
        save_cache(url, content)
    return content


def process_css_file(styles, replacer: Callable[[str], str]):
    def sub(m):
        style_url = m.group(1)
        return 'url("{}")'.format(replacer(style_url))

    return re.sub("""url\(['"]?([^"']*)['"]?\)""", sub, styles)


def base64_encode(mime: str, data: bytes):
    return "data:{mime};base64,{data}".format(mime=mime, data=base64.b64encode(data).decode("ascii"))


def load_module(file_path):
    with open(file_path, "rb") as f:
        name = os.path.splitext(file_path)
        name = os.path.basename(name[0])

        for m in sys.modules.values():
            if getattr(m, "__file__", "") == os.path.realpath(file_path):
                return m

        return imp.load_module(name, f, file_path, ('.py', 'U', 1))


def write_file(path: str, content: Union[str, bytes]):
    if isinstance(content, str):
        with open(path, "wt") as f:
            f.write(content)
    elif isinstance(content, bytes):
        with open(path, "wb") as f:
            f.write(content)


def read_file(path: str):
    with open(path, "rt") as f:
        return f.read()


def read_binary_file(path: str):
    with open(path, "rb") as f:
        return f.read()


log_stack = 0


@contextmanager
def logger_done(text: str):
    global log_stack

    if log_stack >= 1:
        print("(", flush=True, end="")

    print("{}... ".format(text), flush=True, end="")

    failed = False
    try:
        log_stack += 1
        yield
    except:
        failed = True
        raise
    finally:
        print("error" if failed else "done", flush=True, end="")

        if log_stack > 1:
            print(") ", flush=True, end="")

        log_stack -= 1

        if log_stack == 0:
            print()


@contextmanager
def logger_done_multiline(text: str):
    print("{}... ".format(text), flush=True)

    failed = False
    try:
        yield
    except:
        failed = True
        raise
    finally:
        print(text + " " + ("error" if failed else "done"), flush=True)
