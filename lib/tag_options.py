import re

# match [tag=value option1=value]
re_with_main = re.compile("\[[^ =]+=([^ ]+)( [^\]]+)?\]")

# match [tag option1=value]
re_only_opts = re.compile("\[[^ =]+ ([^\]]+)\]")


class TagOptions:
    main = None
    opts = None

    def __init__(self):
        self.opts = {}

    def _parse_opt(self, x):
        p = x.split("=", 1)
        if len(p) == 1:
            self.opts[p[0]] = True
        else:
            self.opts[p[0]] = p[1]

    def has_opt(self, name):
        return name in self.opts

    def get_opt(self, name, default=None):
        return self.opts.get(name, default)

    def get_main_opt(self):
        return self.main

    def merge(self, opts):
        for k, v in opts.opts.items():
            self.opts[k] = v

    @staticmethod
    def parse(txt):
        o = TagOptions()

        txt = txt.replace('\\"', "\0")

        # print("TXT", txt)

        maps = []

        def repl(m):
            nonlocal  maps
            t = m.group(1)
            maps.append(t)
            return "\1{0}\1".format(len(maps) - 1)

        txt = re.sub(r'"([^"]*)"', repl, txt)

        txt = txt.replace("\0", '"')
        maps = [x.replace("\0", '"') for x in maps]
        # print("M", txt)

        if txt is not None:
            opts = None
            m = re_with_main.match(txt)
            if m:
                o.main = m.group(1)
                opts = m.group(2)

            m = re_only_opts.match(txt)
            if m:
                opts = m.group(1)

            if opts:
                # print(opts)
                # opts += " "
                # m = re.search(r'(([a-b]+)="?([^"]*)"? )+', opts)
                # print(m.groups())
                # exit(1)
                for x in opts.split(" "):
                    # o._parse_opt(x)
                    p = x.split("=", 1)
                    if len(p) == 1:
                        o.opts[p[0]] = True
                    else:
                        v = p[1]
                        if v[0] == "\1":
                            v = v[1:-1]
                            v = maps[int(v)]
                        o.opts[p[0]] = v
        return o
