from lib.component import Component
from lib.context import Context


class DeferredScriptComponent(Component):
    def __init__(self, ctx: Context):
        super().__init__(ctx, replace_tag=True)

    def process(self):
        self.set_var("inner_content", self.get_inner_content())
        pass
