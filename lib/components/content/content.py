import textwrap

from lib.component import Component
from lib.context import Context


class ContentComponent(Component):
    def __init__(self, ctx: Context):
        super().__init__(ctx, replace_tag=False)
        self.src = None
        self.content = None

    def process(self):
        cnt = self.get_inner_content()
        if cnt is None or len(cnt) == 0:
            cnt = self.content
        cnt = textwrap.dedent(cnt)
        html = self.ctx.g.content_formatter.format(cnt)
        return html
