var imagesIo = new IntersectionObserver(
  entries => {
    entries.forEach(entry => {
      const img = entry.target;
      if (entry.isIntersecting) {
        loadImage(img);
        imagesIo.unobserve(img);
      }
    });
  },
  {},
);

window.addEventListener('DOMContentLoaded', function () {
  var cache = JSON.parse(window.localStorage.getItem("lazyimage_cache") || "{}");

  var images = document.querySelectorAll("img[target-src]");
  images.forEach(function (img) {
    var targetImage = img.getAttribute("target-src");
    if (!targetImage || targetImage.indexOf("data:") === 0)
      return;

    if (cache.hasOwnProperty(targetImage)) {
      img.parentNode.classList.remove("blurred");
      img.src = targetImage;
    } else if (img.classList.contains("if-visible")) {
      imagesIo.observe(img);
    }
  });
}, false);

window.addEventListener('load', function () {

  var images = document.querySelectorAll("img[target-src]");
  images.forEach(function (img) {
    if (!img.classList.contains("if-visible"))
      loadImage(img);
  });

});

function loadImage(img) {
  var targetImage = img.getAttribute("target-src");
  img.src = targetImage;
  img.addEventListener('load', function () {
    img.parentNode.classList.remove("blurred");

    var cache = JSON.parse(window.localStorage.getItem("lazyimage_cache") || "{}");
    cache[targetImage] = 1;
    window.localStorage.setItem("lazyimage_cache", JSON.stringify(cache));
  });
}
