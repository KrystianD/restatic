from lib.component import Component
from lib.context import Context
from lib.utils import resize_with_aspect


class LazyImageComponent(Component):
    def __init__(self, ctx: Context):
        super().__init__(ctx)
        self.src = None
        self.width = None
        self.height = None
        self.lazy_load = True
        self.target_src = None

    def process(self):
        image_src = self.ctx.g.find_asset(self.src)

        original_image_size = self.ctx.g.ic.get_image_size(image_src)

        tag_image_size = resize_with_aspect(*original_image_size, self.width, self.height)
        w, h = tag_image_size

        print(image_src, "tag_image_size", original_image_size, w, h)
        image = self.ctx.g.ic.get_image(image_src, w, h)

        if self.lazy_load is True or self.lazy_load == "visible":
            lq_image = self.ctx.g.ic.get_low_quality_image(image_src)
            self.width = tag_image_size[0]
            self.height = tag_image_size[1]
            self.target_src = image.url
            self.src = lq_image.url
            self.host_classes.add("blurred")
        else:
            self.src = image.url
            self.replace_tag = True
            self.target_src = ""

    @staticmethod
    def get_tag_name():
        return "LazyImage"
