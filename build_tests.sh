#!/bin/bash
rm -rf output/
python generate.py -s tests/content -o output/content
python generate.py -s tests/fonts -o output/fonts
python generate.py -s tests/images -o output/images
python generate.py -s tests/media-query -o output/media-query
python generate.py -s tests/scripts -o output/scripts
python generate.py -s tests/styles -o output/styles
python generate.py -s tests/components -o output/components
