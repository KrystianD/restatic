import traceback
import time
import threading

from bs4.builder._htmlparser import HTMLParserTreeBuilder
from livereload.handlers import LiveReloadHandler
from livereload.watcher import Watcher

from lib.generator import Generator

HTMLParserTreeBuilder.cdata_list_attributes["*"].remove("class")

last_change_time = None
g: Generator = None


def examine(self):
    # self.filepath = None
    for path in self._tasks:
        item = self._tasks[path]
        if self.is_changed(path, item['ignore']):
            print("path", path, self.filepath)
            if ".cache/" in self.filepath:
                continue
            func = item['func']
            func and func()
    return None, None


Watcher.examine = examine


def main():
    global last_change_time, g
    import argparse
    from livereload import Server

    argparser = argparse.ArgumentParser()
    argparser.add_argument('-s', '--source-path', type=str, metavar="PATH", required=True)
    argparser.add_argument('-o', '--output-dir', type=str, metavar="PATH", required=True)
    argparser.add_argument('-m', '--release', action='store_true')
    argparser.add_argument('-c', '--configuration', type=str)
    argparser.add_argument('-w', '--watch', action='store_true')
    argparser.add_argument('--no-cache', action='store_true')
    argparser.add_argument('--limit', action='append')
    args = argparser.parse_args()

    g = Generator(no_cache=args.no_cache)

    cfg = "debug"
    if args.release:
        cfg = "release"
    if args.configuration:
        cfg = args.configuration

    def compile(exit_on_raise=False):
        try:
            g.generate_from_dir(args.source_path, args.output_dir, configuration_name=cfg, limit_paths=args.limit)
        except:
            if exit_on_raise:
                raise
            else:
                traceback.print_exc()

    if args.watch:
        compile(exit_on_raise=False)

        def thread_func():
            global last_change_time
            while True:
                if last_change_time is not None and time.time() - last_change_time > 0.5:
                    compile(exit_on_raise=False)
                    LiveReloadHandler.reload_waiters()
                    last_change_time = None
                else:
                    time.sleep(0.3)

        builder = threading.Thread(target=thread_func)
        builder.daemon = True
        builder.start()

        def on_change():
            global last_change_time
            last_change_time = time.time()

        server = Server()
        server.watch(args.source_path, on_change, delay="forever")
        server.serve(root=args.output_dir, host='0.0.0.0')
    else:
        compile(exit_on_raise=True)


if __name__ == "__main__":
    main()
