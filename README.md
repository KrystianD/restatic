restatic
=========

Reusable Static Websites generator - Tool for generating highly optimized static modular websites.

Preparation for usage and development
=========
Requirements: NPM, python3.6 and virtualenv.

```bash
virtualenv3 -p python3.6 env36
source env36/bin/activate
pip install -r requirements.txt
npm install
```

Usage
=========
```bash
python generate.py --source-path site_source/ --output-dir output/ [--release] [--watch]
```

Testing
=========
```bash
./build_tests.sh
```
